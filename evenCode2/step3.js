function add(string) {
    const character = string.replace(/\n+/g, ',');
    const array = character.split(",").map(Number)
  
    return array.reduce((a, b) => a + b);
}

console.log(add('\n\n20\n10\n\n\n\n\n2,3,4,15\n'));
