function add(string) {
    const array = string.split(",").map(Number)
    return array.reduce((a, b) => a + b);
}

console.log(add('10,2,4,6'))
