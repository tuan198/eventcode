//     (1, -2), (-2, 1), (2, -1), (-1, 2), 
//     (2, 1), (1, 2), (-2, -1), (-1, -2) 

// Chess kích thước bàn cờ chess x chess.
// x tọa độ hàng.
// y tọa độ cột.

function problem(chess, x, y) {
    const row = [1,-2,2,-1,2,1,-2,-1];
    const column = [-2,1,-1,2,1,2,-1,-2];
    var total = 0;
    
    if (x > 0 && x <= chess && y > 0 && y <= chess) {
        for (let i = 0; i < 8; i++) {
            var h = x + row[i];
            var c = y + column[i];
            var m = [h, c]
            if (h > 0 && h <= chess && c > 0 && c <= chess) {
                console.log(m)
                total++
            }
        }
        return "Tổng số nước đi hợp lệ: " + total;
    }
    return 'tọa độ x, y không hợp lệ';
}

console.log(problem(1000, 999, 4));
