const arrayDiff = (a, b) => {
    const total = a.filter(x => !b.includes(x));
    return total;
}

console.log(arrayDiff([], [4, 5])); // -> []
console.log(arrayDiff([3, 4], [3])); // -> [4]
console.log(arrayDiff([1, 8, 2], [])); // -> [1, 8, 2]
console.log(arrayDiff([1, 2, 2, 2, 3], [2])); // -> [1, 3]
console.log(arrayDiff([1, 2, 5, 6, 8, 9], [2, 6])) // -> [1, 5, 8, 9]
